<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;

class CategoryController extends Controller
{
    public function showCategory(string $alias){
        $category = Category::where(['alias' => $alias])->first();
        $products = $category->products();

        // show count products on page
        $countProducts = count($products);

        return view('category.categories', [
            'category' => $category,
            'products' => $products,
            'countProducts' => $countProducts
        ]);
    }
}
