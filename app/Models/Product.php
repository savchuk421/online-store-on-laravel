<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    public function images() {
        return $this->hasMany(ProductImage::class);
    }

    protected function getFirstImageAttribute(): string
    {
        $imagePath = $this->images[0]['img'] ?? 'no-image.png';
        $imageDir = '/images/';

        return $imageDir . $imagePath;
    }

    protected function getPriceAttribute($price): string
    {
        return '$'.number_format((float) $price, 2, '.', '');
    }

    public function category() {
        return $this->belongsTo(Category::class);
    }



}
