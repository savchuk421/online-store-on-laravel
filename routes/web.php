<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

Route::get('/', [HomeController::class, 'index']);
Route::get('/{cat}', [\App\Http\Controllers\CategoryController::class, 'showCategory'])->name('showCategory');
Route::get('/{cat}/{product_id}', [\App\Http\Controllers\ProductController::class, 'show'])->name('showProduct');

